# SiLA Standard
SiLA’s mission is to establish international standards which create open connectivity in lab automation. SiLA’s vision is to create interoperability, flexibility and resource optimization for laboratory instruments integration and software services based on standardized communication protocols and content specifications. SiLA promotes open standards to allow integration and exchange of intelligent systems in a cost effective way.

The SiLA 2 specification is a multi part specification and the work-in-progress
documents can be accessed on google drive:

* [Part (A) - Overview, Concepts and Core Specification](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit)
* [Part (B) - Mapping Specification](https://docs.google.com/document/d/1-shgqdYW4sgYIb5vWZ8xTwCUO_bqE13oBEX8rYY_SJA/edit)
* [Part (C) - Features Index](https://docs.google.com/document/d/1J9gypD6HofLQZ8cPgLWljRuO0V8l5dS22TWQxFy4bhY/edit)

For more information, visit our [website](http://sila-standard.com/).

In case of general questions, contact either Max ([max@unitelabs.ch](mailto:max@unitelabs.ch)) or 
Daniel ([daniel.juchli@sila-standard.org](mailto:daniel.juchli@sila-standard.org)).

## SiLA Organisation
Everyone is open to contribute, we organise code issues with GitLab and have weekly meetings of the 
SiLA 2 Working group that is accessible to members. There is a public slack channel that anyone can 
join to ask for help and/or give feedback: [slack link](https://join.slack.com/t/sila-standard/shared_invite/enQtNDI0ODcxMDg5NzkzLTc4YjdkNzgxYjM5NDIyMzAyNTJjMjE1ZWI5MzY0M2Y2NmY3ZGQ2NTI3YzJiMmIzNTFmZmJkMWI3ZTMyMTk5NGY).

## SiLA Ecosystem
Various tools are published by different companies both for SiLA, but also for gRPC. For gRPC you can find 
many links [here](https://github.com/grpc-ecosystem/awesome-grpc).

For SiLA 2 the current tools are accessible, in addition to the reference implementations found in this GitLab group:

* UniteLabs is publishing its own demo and development tools on: [download link](https://unitelabs.gitlab.io/unitelabs_base/)
* TU Berlin published a web-based feature viewer: [download link](https://gitlab.tu-berlin.de/haenser/SiLA_FeatureDefinitionViewer)

# SiLA Base
This repository contains base definitions of the SiLA standard, such as the feature schemas and framework 
protos. Additionally it contains the SiLA Features of the standardization group.

## Style Guidelines
The tabbing in the xml documents is done with 2 spaces.

# License
This code is licensed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License)